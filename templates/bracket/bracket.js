var match_results = [0,0,0,0,0,0,0,0,0,0,0,0,0];
var team_names = [{{ tournament.team_names|safe }}];

function showTeams() {
	if(document.getElementsByClassName("team 1 loser").length > 0) {
		document.getElementById("5_1").innerHTML = document.getElementsByClassName("team 1 loser")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 2 loser").length > 0) {
		document.getElementById("5_2").innerHTML = document.getElementsByClassName("team 2 loser")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 3 loser").length > 0) {
		document.getElementById("6_1").innerHTML = document.getElementsByClassName("team 3 loser")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 4 loser").length > 0) {
		document.getElementById("6_2").innerHTML = document.getElementsByClassName("team 4 loser")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 1 winner").length > 0) {
		document.getElementById("7_1").innerHTML = document.getElementsByClassName("team 1 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 2 winner").length > 0) {
		document.getElementById("7_2").innerHTML = document.getElementsByClassName("team 2 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 3 winner").length > 0) {
		document.getElementById("8_1").innerHTML = document.getElementsByClassName("team 3 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 4 winner").length > 0) {
		document.getElementById("8_2").innerHTML = document.getElementsByClassName("team 4 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 7 loser").length > 0) {
		document.getElementById("9_1").innerHTML = document.getElementsByClassName("team 7 loser")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 5 winner").length > 0) {
		document.getElementById("9_2").innerHTML = document.getElementsByClassName("team 5 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 8 loser").length > 0) {
		document.getElementById("10_1").innerHTML = document.getElementsByClassName("team 8 loser")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 6 winner").length > 0) {
		document.getElementById("10_2").innerHTML = document.getElementsByClassName("team 6 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 7 winner").length > 0) {
		document.getElementById("11_1").innerHTML = document.getElementsByClassName("team 7 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 10 winner").length > 0) {
		document.getElementById("11_2").innerHTML = document.getElementsByClassName("team 10 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 8 winner").length > 0) {
		document.getElementById("12_1").innerHTML = document.getElementsByClassName("team 8 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 9 winner").length > 0) {
		document.getElementById("12_2").innerHTML = document.getElementsByClassName("team 9 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 11 winner").length > 0) {
		document.getElementById("13_1").innerHTML = document.getElementsByClassName("team 11 winner")[0].innerHTML;
	}
	if(document.getElementsByClassName("team 12 winner").length > 0) {
		document.getElementById("13_2").innerHTML = document.getElementsByClassName("team 12 winner")[0].innerHTML;
	}
	if(page == "show_bracket") {
		document.getElementById("id_bracket_results").value = match_results.join("");
	}
}

$(document).ready(function(){
	document.getElementById("1_1").innerHTML = team_names[0];
	document.getElementById("1_2").innerHTML = team_names[1];
	document.getElementById("2_1").innerHTML = team_names[2];
	document.getElementById("2_2").innerHTML = team_names[3];
	document.getElementById("3_1").innerHTML = team_names[4];
	document.getElementById("3_2").innerHTML = team_names[5];
	document.getElementById("4_1").innerHTML = team_names[6];
	document.getElementById("4_2").innerHTML = team_names[7];
	if(page == "show_submit") {
		match_results = "{{ submit.predicted_results }}".split("");
	} else if(page == "show_bracket") {
		match_results = document.getElementById("id_bracket_results").value.split("");
	} else if(page == "tournament_results") {
		match_results = "{{ tournament.match_results }}".split("");
	}
	showTeams();
	$('td.team').click(function(){
		this.className = this.className.split(" ")[0] + " " + this.className.split(" ")[1] + " winner";
			document.getElementById(this.id + "_res").innerHTML="W";
			document.getElementById(this.id + "_res").className="W";
		var loserId = this.id.slice(0, -1);
		if(this.id.slice(-1) == "1") {
			loserId = loserId.concat("2");
		}
		else {
			loserId = loserId.concat("1");
		}
		document.getElementById(loserId).className = "team " + loserId.slice(0, -2) + " loser";
			document.getElementById(loserId + "_res").innerHTML = "";
			document.getElementById(loserId + "_res").className = "L";
		match_results[parseInt(this.id.slice(0, -2))-1] = parseInt(this.id.slice(-1));
		showTeams();
	});
		for(var i = 0; i < 13; i++) {
			if(match_results[i] != 0) {
				var winnerId = "#" + (i+1).toString() + "_" + match_results[i].toString();
				$(winnerId).trigger('click');
			}
		}
		if(page != "show_bracket") {
			$('td.team').off('click');
		}
});
