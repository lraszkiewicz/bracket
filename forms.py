from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import validate_slug
from nocaptcha_recaptcha.fields import NoReCaptchaField

from .models import SubmittedBracket, Tournament

def validate_all_results(results):
    if '0' in results:
        raise ValidationError(
            ("You did not choose the result of every match.")
        )

class BracketForm(forms.Form):
    bracket_results = forms.CharField(
        label='Bracket results',
        max_length=30,
        widget=forms.HiddenInput(),
        initial="0000000000000",
        validators=[validate_all_results]
    )
    captcha = NoReCaptchaField(
        error_messages={'required': 'You must solve the Captcha!'}
    )
