from django.contrib import admin

from .models import Tournament, SubmittedBracket

def update_scores(modeladmin, request, queryset):
    for submit in queryset:
        submit.update_score()

class SubmitAdmin(admin.ModelAdmin):
    readonly_fields = ['time_sent']
    fields = ['nick', 'tournament', 'predicted_results', 'score', 'time_sent']
    list_display = ['nick', 'tournament', 'score', 'time_sent']
    list_filter = ['tournament']
    actions = [update_scores]

admin.site.register(Tournament)
admin.site.register(SubmittedBracket, SubmitAdmin)
