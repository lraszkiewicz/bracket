from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect, render_to_response
from django.views import generic
from django.utils import timezone
from django.contrib.auth import logout

from .models import Tournament, SubmittedBracket
from .forms import BracketForm

import sys

def logout_view(request):
    logout(request)
    return redirect('bracket:index')

def index(request):
    tournament_list = Tournament.objects.order_by('-deadline')
    return render(request, 'bracket/index.html',
        {'tournament_list': tournament_list}
    )

def leaderboard(request, tournament_shortname):
    tournament = get_object_or_404(Tournament, shortname=tournament_shortname)
    submit_list = SubmittedBracket.objects.filter(tournament=tournament).order_by('-score')
    submit_list_with_places = []
    for i, submit in enumerate(submit_list):
        if i == 0:
            submit_list_with_places.append((submit, str(i+1)+'.'))
        else:
            if submit.score != submit_list_with_places[-1][0].score:
                submit_list_with_places.append((submit, str(i+1)+'.'))
            else:
                submit_list_with_places.append((submit, ''))
    return render(request, 'bracket/leaderboard.html',
        {'submit_list': submit_list_with_places, 'tournament': tournament}
    )

def tournament_results(request, tournament_shortname):
    tournament = get_object_or_404(Tournament, shortname=tournament_shortname)
    return render(request,
        'bracket/tournament_results.html', {'tournament': tournament}
    )

def show_tournament(request, tournament_shortname):
    tournament = get_object_or_404(Tournament, shortname=tournament_shortname)
    if timezone.now() <= tournament.deadline:
        if request.method == 'POST':
            form = BracketForm(request.POST)
            if timezone.now() <= tournament.deadline:
                if form.is_valid():
                    SubmittedBracket.objects.filter(tournament=tournament, nick=request.user.username).delete()
                    submit = SubmittedBracket(tournament=tournament, nick=request.user.username, predicted_results=form.cleaned_data['bracket_results'])
                    submit.save()
                    return redirect('bracket:show_submit', tournament.shortname, request.user.username)
                else:
                    return render(request, 'bracket/show_bracket.html',
                        {'tournament': tournament, 'form': form}
                    )
            else:
                return redirect('bracket:show_tournament', tournament.shortname)
        else:
            form = None
            if request.user.is_authenticated:
                form = BracketForm()
            already_submitted = False
            if SubmittedBracket.objects.filter(tournament=tournament, nick=request.user.username):
                already_submitted = True
            return render(request, 'bracket/show_bracket.html',
                {'tournament': tournament, 'form': form, 'already_submitted': already_submitted}
            )
    else:
        return render(request, 'bracket/deadline.html',
            {'tournament': tournament}
        )

def show_submit(request, tournament_shortname, nickname):
    tournament = get_object_or_404(Tournament, shortname=tournament_shortname)
    submit = get_object_or_404(SubmittedBracket, tournament=tournament, nick=nickname)
    can_change = False
    if submit.nick == request.user.username and timezone.now() <= tournament.deadline:
        can_change = True
    return render(request, 'bracket/show_submit.html',
        {'tournament': tournament, 'submit': submit, 'can_change': can_change}
    )

def show_stats(request, tournament_shortname):
    tournament = get_object_or_404(Tournament, shortname=tournament_shortname)

    import operator
    stats = ''
    brackets = SubmittedBracket.objects.filter(tournament=tournament)
    n = brackets.count()
    teams = tournament.team_names[1:-1].split('","')
    d = dict()
    for team in teams:
        d[team] = 0
    dictlist = [dict(d) for x in range(13)]

    for bracket in brackets:
        winners = bracket.tournament.generateMatches(bracket.predicted_results)[1]
        for i in range(13):
            dictlist[i][winners[i]] += 1

    i=0
    for di in dictlist:
        i += 1
        stats += 'Match ' + str(i) + ' - '
        for team in teams:
            if di[team] == 0:
                del di[team]
            else:
                di[team] = 100.0*di[team]/n
        # s = sorted(di.items(), key=operator.itemgetter(1), reverse=True)
        s = sorted(di.items(), key=lambda x: (-x[1], x[0].lower()))
        for t in s:
            stats += t[0] + ': ' + ('%0.2f' % t[1]) + '%, '
        stats = stats[:-2]
        stats += '<br />'
    stats += '<br />'

    dictlist = [dict(d) for x in range(13)]
    for bracket in brackets:
        bracket_matches = bracket.tournament.generateMatches(bracket.predicted_results)
        real_matches = bracket.tournament.generateMatches(bracket.tournament.match_results)
        for i in range(13):
            if sorted(bracket_matches[0][i]) == sorted(real_matches[0][i]):
                dictlist[i][bracket_matches[1][i]] += 1

    i=0
    for di in dictlist:
        s = 0
        for team in teams:
            s += di[team]
        i += 1
        stats += 'Match ' + str(i) + ' (' + str('%0.2f' % (100.0*s/n)) + '%)' + ' - '
        for team in teams:
            if di[team] == 0:
                del di[team]
            else:
                di[team] = 100.0*di[team]/s
        s = sorted(di.items(), key=operator.itemgetter(1), reverse=True)
        for t in s:
            stats += t[0] + ': ' + ('%0.2f' % t[1]) + '%, '
        stats = stats[:-2]
        stats += '<br />'
    stats += '<br />'

    dictlist = [dict(d) for x in range(13)]
    for bracket in brackets:
        bracket_matches = bracket.tournament.generateMatches(bracket.predicted_results)
        real_matches = bracket.tournament.generateMatches(bracket.tournament.match_results)
        for i in range(10,12):
            for j in range(10,12):
                if sorted(bracket_matches[0][i]) == sorted(real_matches[0][j]):
                    dictlist[j][bracket_matches[1][i]] += 1

    i=0
    for di in dictlist:
        i += 1
        if i == 11 or i == 12:
            s = 0
            for team in teams:
                s += di[team]
            stats += 'Match ' + str(i) + ' (' + str('%0.2f' % (100.0*s/n)) + '%)' + ' - '
            for team in teams:
                if di[team] == 0:
                    del di[team]
                else:
                    di[team] = 100.0*di[team]/s
            s = sorted(di.items(), key=operator.itemgetter(1), reverse=True)
            for t in s:
                stats += t[0] + ': ' + ('%0.2f' % t[1]) + '%, '
            stats = stats[:-2]
            stats += '<br />'

    return render(request,
        'bracket/show_stats.html', {'tournament': tournament, 'stats': stats}
    )
