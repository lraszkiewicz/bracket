from django.conf.urls import url

from . import views

app_name = 'bracket'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^logout/', views.logout_view, name='logout_view'),
    url(r'^(?P<tournament_shortname>[a-zA-Z0-9_.-]+)/$', views.show_tournament, name='show_tournament'),
    url(r'^(?P<tournament_shortname>[a-zA-Z0-9_.-]+)/leaderboard/$', views.leaderboard, name='leaderboard'),
    url(r'^(?P<tournament_shortname>[a-zA-Z0-9_.-]+)/stats/$', views.show_stats, name='show_stats'),
    url(r'^(?P<tournament_shortname>[a-zA-Z0-9_.-]+)/tournament_results/$', views.tournament_results, name='tournament_results'),
    url(r'^(?P<tournament_shortname>[a-zA-Z0-9_.-]+)/submit/(?P<nickname>[a-zA-Z0-9_.-]+)/$', views.show_submit, name='show_submit'),
]
