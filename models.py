from django.db import models

class Tournament(models.Model):
    name = models.CharField(max_length=100)
    shortname = models.CharField(max_length=50)
    bracket_type = models.IntegerField(default=0)
    # type 0: 8 teams, 2 groups, groups double elimination, play-offs single elimination
    # type 1: same as above with different semi-final scoring
    team_names = models.CharField(max_length=300) # comma separated, each team in quatation marks
    match_results = models.CharField(max_length=20)
    last_results_update = models.DateTimeField(auto_now=True)
    deadline = models.DateTimeField()
    additional_text = models.TextField(default="", blank=True)
    scoring = models.TextField(default="", blank=True)

    def generateMatches(self, results):
        matches = [
            [], [], [], [],
            ['1 L', '2 L'], ['3 L', '4 L'],
            ['1 W', '2 W'], ['3 W', '4 W'],
            ['7 L', '5 W'], ['8 L', '6 W'],
            ['7 W', '10 W'], ['8 W', '9 W'],
            ['11 W', '12 W']
        ]
        team_names = self.team_names[1:-1].split('","')
        if self.bracket_type == 0 or self.bracket_type == 1:
            playing = [['',''] for i in range(13)]
            winners = ['']*13
            losers = ['']*13
            for i in range(0,4):
                playing[i] = [team_names[i*2], team_names[i*2+1]]
                if results[i] == '1':
                    winners[i] = playing[i][0]
                    losers[i] = playing[i][1]
                elif results[i] == '2':
                    winners[i] = playing[i][1]
                    losers[i] = playing[i][0]
            for i in range(4,13):
                match1 = matches[i][0].split()
                match2 = matches[i][1].split()
                if match1[1] == 'W':
                    playing[i][0] = winners[int(match1[0])-1]
                elif match1[1] == 'L':
                    playing[i][0] = losers[int(match1[0])-1]
                if match2[1] == 'W':
                    playing[i][1] = winners[int(match2[0])-1]
                elif match1[1] == 'L':
                    playing[i][1] = losers[int(match2[0])-1]
                if results[i] == '1':
                    winners[i] = playing[i][0]
                    losers[i] = playing[i][1]
                elif results[i] == '2':
                    winners[i] = playing[i][1]
                    losers[i] = playing[i][0]
            return playing, winners, losers

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        super(Tournament, self).save(*args, **kwargs)
        submits = SubmittedBracket.objects.filter(tournament=self)
        for submit in submits:
            submit.update_score()


class SubmittedBracket(models.Model):
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    nick = models.CharField(max_length=25)
    predicted_results = models.CharField(max_length=20)
    time_sent = models.DateTimeField(auto_now_add=True)
    score = models.IntegerField(default=0)

    def update_score(self):
        submit_winners = self.tournament.generateMatches(self.predicted_results)[1]
        actual_winners = self.tournament.generateMatches(self.tournament.match_results)[1]
        new_score = 0
        if self.tournament.bracket_type == 0:
            points = [2, 2, 2, 2, 3, 3, 4, 4, 4, 4, 8, 8, 16]
            for i in range(0,13):
                if actual_winners[i] != None and submit_winners[i] == actual_winners[i]:
                    new_score = new_score + points[i]
        elif self.tournament.bracket_type == 1:
            points = [2, 2, 2, 2, 3, 3, 4, 4, 4, 4, 8, 8, 16]
            for i in range(0,10):
                if actual_winners[i] != None and submit_winners[i] == actual_winners[i]:
                    new_score = new_score + points[i]
            if actual_winners[10] != None and (submit_winners[10] == actual_winners[10] or submit_winners[11] == actual_winners[10]):
                new_score = new_score + points[10]
            if actual_winners[11] != None and (submit_winners[10] == actual_winners[11] or submit_winners[11] == actual_winners[11]):
                new_score = new_score + points[11]
            if actual_winners[12] != None and submit_winners[12] == actual_winners[12]:
                new_score = new_score + points[12]
        self.score = new_score
        self.save()

    def __str__(self):
        return self.nick + " " + self.predicted_results + " " + self.tournament.shortname
